package com.example.alex.musicplayerservice;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by alex on 17.08.2017.
 */

public class SongsListAdapter extends CursorAdapter {

    final String LOG_TAG = "myLog";
    static Intent intent;
    LayoutInflater layoutInflater;
    MyMusicServiceResultReceiver myMusicServiceResultReceiver;

    public SongsListAdapter(Context context, Cursor c, boolean autoRequery,
                            MyMusicServiceResultReceiver myMusicServiceResultReceiver) {
        super(context, c, autoRequery);
        this.myMusicServiceResultReceiver = myMusicServiceResultReceiver;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return layoutInflater.inflate(R.layout.item_song, parent, false);
    }

    @Override
    public void bindView(View view, Context context,  Cursor cursor) {
        TextView tvSongName;
        TextView tvArtistName;

        tvSongName = (TextView) view.findViewById(R.id.tvSongName);
        tvSongName.setText(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)));

        tvArtistName = (TextView) view.findViewById(R.id.tvArtistName);
        tvArtistName.setText(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
         + " + " + cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION)));
    }
}
