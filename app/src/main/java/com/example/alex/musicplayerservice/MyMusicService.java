package com.example.alex.musicplayerservice;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.os.ResultReceiver;
import android.util.Log;
import android.widget.RemoteViews;

import java.io.IOException;

/**
 * Created by alex on 19.08.2017.
 */

public class MyMusicService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {

    final String LOG_TAG = "myLog";

    private MediaPlayer mediaPlayer = null;
    private ResultReceiver myMusicServiceResultReceiver;
    private Uri playing_Uri;
    private Notification notificationBar;

    private String songName;
    private String artist;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "MyMusicService is created");
        //initPlayer();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction() == MainActivity.MP_PAUSE && mediaPlayer != null) {
            mediaPlayer.pause();
            sendInfoToActivity(MainActivity.MP_PAUSE, 1);
            showNotification(MainActivity.MP_START);
        }
        //Play new song
        if (intent.getAction() == MainActivity.MP_START_NEW) {
            try {
                playing_Uri = intent.getParcelableExtra("songUri");
                myMusicServiceResultReceiver = intent.getParcelableExtra(MainActivity.MUSIC_RECEIVER);
                songName = null;
                artist = null;
                songName = intent.getStringExtra(MainActivity.SONG_NAME);
                artist = intent.getStringExtra(MainActivity.ARTIST);

                initPlayer();
                //
                Bundle bundle = new Bundle();
                bundle.putString(MainActivity.PLAYING_URI, playing_Uri.toString());
                bundle.putString(MainActivity.BUNDLE_ACTION, MainActivity.MP_START_NEW);
                sendInfoToActivity(bundle, MainActivity.MP_START_NEW, 1);

                mediaPlayer.setDataSource(getApplicationContext(), playing_Uri);
                mediaPlayer.setOnPreparedListener(this);
                mediaPlayer.setOnCompletionListener(this);
                mediaPlayer.prepareAsync();
                showNotification(MainActivity.MP_PAUSE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //Play song after pause
        if (intent.getAction() == MainActivity.MP_START) {
            if (mediaPlayer != null) {
                if (mediaPlayer.getCurrentPosition() != 0) {
                    mediaPlayer.start();
                } else {
                    //Start from current song if it exists;
                    Uri tmpUri = intent.getParcelableExtra("songUri");
                    if (tmpUri != null && playing_Uri != tmpUri) {
                        try {
                            playing_Uri = tmpUri;
                            mediaPlayer.setDataSource(getApplicationContext(), playing_Uri);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        mediaPlayer.setOnPreparedListener(this);
                        mediaPlayer.setOnCompletionListener(this);
                        mediaPlayer.prepareAsync();
                    }
                }
                showNotification(MainActivity.MP_PAUSE);
                sendInfoToActivity(MainActivity.MP_START_NEW, 1);
            }
        }
        if (intent.getAction() == MainActivity.MP_SONG_FINISH){
            sendInfoToActivity(MainActivity.MP_SONG_FINISH, 1);
        }
        if (intent.getAction() == MainActivity.MP_CLOSE){
            mediaPlayer.stop();
            sendInfoToActivity(MainActivity.MP_CLOSE, 1);
            stopSelf();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void initPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }

    @Override
    public void onDestroy() {
        if (mediaPlayer != null) mediaPlayer.release();
        Log.d(LOG_TAG, "MyMusicService is destroyed");
        super.onDestroy();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        //When the song has finished
        sendInfoToActivity(MainActivity.MP_SONG_FINISH, 1);
    }

    private void sendInfoToActivity(String action, int resultCode){
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.BUNDLE_ACTION, action);
        myMusicServiceResultReceiver.send(resultCode, bundle);
    }
    private void sendInfoToActivity(Bundle bundle, String action, int resultCode){
        bundle.putString(MainActivity.BUNDLE_ACTION, action);
        myMusicServiceResultReceiver.send(resultCode, bundle);
    }

    private void showNotification(@Nullable String action) {
        //Using RemoteView to bind CustomLayout into Notification
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.status_bar);


        //Open MainActivity
        Intent openActivityIntent = new Intent(this, MainActivity.class);
        //openActivityIntent.setAction(action);
        openActivityIntent.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
        PendingIntent popenActivityIntent = PendingIntent.getActivity(this, 0, openActivityIntent, 0);


        //Play or pause
        Intent playPauseIntent = new Intent(this, MyMusicService.class);
        if (action == MainActivity.MP_PAUSE) {
            remoteViews.setImageViewResource(R.id.ibBarPlayPause, R.drawable.ic_pause_circle_outline_orange_24dp);
        } else if (action == MainActivity.MP_START) {
            remoteViews.setImageViewResource(R.id.ibBarPlayPause, R.drawable.ic_play_circle_outline_orange_24dp);
        }
        playPauseIntent.setAction(action);
        PendingIntent pplayPauseIntent = PendingIntent.getService(this, 0, playPauseIntent, 0);

        //Play next song
        Intent nextSongIntent = new Intent(this, MyMusicService.class);
        nextSongIntent.setAction(MainActivity.MP_SONG_FINISH);
        PendingIntent pnextSongIntent = PendingIntent.getService(this, 0, nextSongIntent, 0);

        //Close player
        Intent closeIntent = new Intent(this, MyMusicService.class);
        closeIntent.setAction(MainActivity.MP_CLOSE);
        PendingIntent pcloseIntent = PendingIntent.getService(this, 0, closeIntent, 0);


        remoteViews.setOnClickPendingIntent(R.id.ibBarPlayPause, pplayPauseIntent);
        remoteViews.setOnClickPendingIntent(R.id.ibBarNextSong, pnextSongIntent);
        remoteViews.setOnClickPendingIntent(R.id.ibBarClose, pcloseIntent);
        remoteViews.setTextViewText(R.id.tvBarSongName, (songName != null) ? songName : "Unknown song");
        remoteViews.setTextViewText(R.id.tvBarArtist, (artist != null) ? artist : "Unknown artist");

        notificationBar = new Notification.Builder(this).build();
        notificationBar.contentView = remoteViews;
        notificationBar.flags = Notification.FLAG_ONGOING_EVENT;
        notificationBar.icon = R.drawable.ic_music_note_white_24dp;
        notificationBar.contentIntent = popenActivityIntent;
        startForeground(101, notificationBar);



      /*
        notificationBar.contentView*/
    }
}
