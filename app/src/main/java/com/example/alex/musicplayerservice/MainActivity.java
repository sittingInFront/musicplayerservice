package com.example.alex.musicplayerservice;

import android.Manifest;
import android.content.ContentUris;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.alex.musicplayerservice.SongsListAdapter.intent;

public class MainActivity extends AppCompatActivity implements MyMusicServiceResultReceiver.Receiver,
        AdapterView.OnItemClickListener {

    final String LOG_TAG = "myLog";
    public static final String MP_SONG_FINISH = "song_finish";
    public final static String MP_PAUSE = "pause";
    public final static String MP_START = "start";
    public final static String MP_INIT = "init";
    public final static String MP_START_NEW = "start_new";
    public final static String MP_CLOSE = "close";
    public final static String BUNDLE_ACTION = "bundle_action";
    public final static String MUSIC_RECEIVER = "music_receiver";
    public final static String PLAYING_URI = "playing_song_uri";
    public final static String ARTIST = "artist";
    public final static String SONG_NAME = "title";
    public final Uri EXTERNAL_CONTENT_URI = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

    private String current_action = MP_INIT;
    private int current_song_position;
    MyMusicServiceResultReceiver myMusicServiceResultReceiver;
    Uri playing_Uri;
    SharedPreferences sPref;
    ListView lwSongsList;
    SongsListAdapter songsListAdapter;
    TextView tvSongName;
    Button btnStartStop;

    private String[] projection = {
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.ARTIST,
            MediaStore.Audio.Media.TITLE,
            MediaStore.Audio.Media.DATA,
            MediaStore.Audio.Media.DISPLAY_NAME,
            MediaStore.Audio.Media.DURATION
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults == null || grantResults[0] == PackageManager.PERMISSION_DENIED
                || grantResults[1] == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else {
            init();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lwSongsList = (ListView) findViewById(R.id.lvSongsList);

        /*If your player application needs to keep the screen from dimming or the processor from sleeping,
        or uses the MediaPlayer.setScreenOnWhilePlaying() or MediaPlayer.setWakeMode() methods, you must request this permission.*/

        int permissionReadExStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWakeLock = ContextCompat.checkSelfPermission(this, Manifest.permission.WAKE_LOCK);
        if (permissionReadExStorage == PackageManager.PERMISSION_DENIED || permissionWakeLock == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WAKE_LOCK}, 1);
        } else {
            init();
        }
        btnStartStop = (Button) findViewById(R.id.btnStartStop);
        tvSongName = (TextView) findViewById(R.id.tvSongName);
        btnStartStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (current_action != MP_INIT) {
                    intent = new Intent(MainActivity.this, MyMusicService.class);
                    intent.putExtra(MUSIC_RECEIVER, myMusicServiceResultReceiver);
                    if (current_action == MP_PAUSE) {
                        intent.putExtra("songUri", playing_Uri);
                        intent.setAction(MP_START);
                    }
                    if (current_action == MP_START_NEW || current_action == MP_START) {
                        intent.setAction(MP_PAUSE);
                    }
                    startService(intent);
                } else {
                    long id = ContentUris.parseId(playing_Uri);
                    current_song_position = getPositionFromID(id);
                    playSongFromID(ContentUris.parseId(playing_Uri));
                }
            }
        });
    }

    private void init() {
        Cursor cursor = getContentResolver().query(EXTERNAL_CONTENT_URI, projection, null, null, null);
        myMusicServiceResultReceiver = new MyMusicServiceResultReceiver(new Handler());
        myMusicServiceResultReceiver.setReceiver(this);
        songsListAdapter = new SongsListAdapter(this, cursor, true, myMusicServiceResultReceiver);
        lwSongsList.setAdapter(songsListAdapter);
        lwSongsList.setOnItemClickListener(this);

        //get last current song uri;
        sPref = getPreferences(MODE_PRIVATE);
        String tmpUri = sPref.getString(PLAYING_URI, "");
        if (tmpUri != "") {
            playing_Uri = Uri.parse(tmpUri);
        }

    }

    @Override
    protected void onResume() {
        //Getting current song uri
        sPref = getPreferences(MODE_PRIVATE);
        String tmpUri = sPref.getString(PLAYING_URI, "");
        if (tmpUri != "") {
            playing_Uri = Uri.parse(tmpUri);
            tvSongName.setText(getSongTitleFromUri(playing_Uri));
        }
        super.onResume();
    }

    private void saveSongUri() {
        //save current song uri
        sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sPref.edit();
        editor.putString(PLAYING_URI, playing_Uri.toString());
        editor.commit();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }


    @Override
    public void onResultReceive(int resultCode, Bundle bundle) {
        Log.d(LOG_TAG, "onResultReceive");
        if (resultCode == 1 && bundle != null) {
            current_action = bundle.getString(BUNDLE_ACTION);
            if (current_action == MP_START_NEW) {
                try {
                    btnStartStop.setBackgroundResource(R.drawable.ic_pause_circle_outline_black_24dp);
                    playing_Uri = Uri.parse(bundle.getString(PLAYING_URI));
                    tvSongName.setText(getSongTitleFromUri(playing_Uri));
                    saveSongUri();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (current_action == MP_PAUSE) {
                btnStartStop.setBackgroundResource(R.drawable.ic_play_circle_outline_black_24dp);
            }
            if (current_action == MP_SONG_FINISH) {
                if (lwSongsList.getCount() > current_song_position) {
                    current_song_position += 1;
                } else {

                    current_song_position = 0;
                }
                long newSongID = lwSongsList.getItemIdAtPosition(current_song_position);
                playSongFromID(newSongID);
            }

            if (current_action == MP_CLOSE) {
                finish();
            }

        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Keeping current_song_position to go next
        current_song_position = position;
        playSongFromID(id);
    }

    private String getSongTitleFromUri(Uri songUri) {
        long _ID = ContentUris.parseId(songUri);
        Cursor cursor = getContentResolver().query(EXTERNAL_CONTENT_URI,
                projection, MediaStore.Audio.Media._ID + " = ?", new String[]{String.valueOf(_ID)}, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                return cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
            }
        }
        return "Unknown";
    }

    private void playSongFromID(long id) {
        Uri songUri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Long.valueOf(id));
        try {
            Cursor cursor = getContentResolver().query(EXTERNAL_CONTENT_URI,
                    projection, MediaStore.Audio.Media._ID + " = ?", new String[]{String.valueOf(id)}, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    tvSongName.setText(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)));
                    //Play new song
                    intent = new Intent(this, MyMusicService.class);
                    intent.putExtra("songUri", songUri);
                    intent.putExtra(ARTIST, cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)));
                    intent.putExtra(SONG_NAME, cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)));
                    intent.putExtra(MainActivity.MUSIC_RECEIVER, myMusicServiceResultReceiver);
                    intent.setAction(MainActivity.MP_START_NEW);
                    startService(intent);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getPositionFromID(long id){
        int i = 0;
        for (i = 0; i < lwSongsList.getCount(); i++){
            if (lwSongsList.getItemIdAtPosition(i) == id) {break;}
        }

        return i;
    }
}
