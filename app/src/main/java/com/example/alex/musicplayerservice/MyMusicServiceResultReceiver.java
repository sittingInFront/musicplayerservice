package com.example.alex.musicplayerservice;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;

/**
 * Created by alex on 20.08.2017.
 */

public class MyMusicServiceResultReceiver extends ResultReceiver {
    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
        receiver.onResultReceive(resultCode, resultData);
    }

    /**
     * Create a new ResultReceive to receive results.  Your
     * {@link #onReceiveResult} method will be called from the thread running
     * <var>handler</var> if given, or from an arbitrary thread if null.
     *
     * @param handler
     */
    private Receiver receiver;

    public interface Receiver {
        void onResultReceive(int resultCode, Bundle bundle);
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public MyMusicServiceResultReceiver(Handler handler) {
        super(handler);
    }
}
